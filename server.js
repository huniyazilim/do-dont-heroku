require('dotenv').config();

var express = require('express');
var bodyParser = require("body-parser");

const booksController = require("./controllers/booksController");
const doDontController = require("./controllers/doDontController");

var app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use((req, res, next) => {
    console.log(`method : ${req.method} url : ${req.url}`);
    req.body.fromNadir = " Hellöööööö";
    next();
});

app.set('port', (process.env.PORT || 5000));

process.on('unhandledRejection', (reason, p) => {
    console.log('Unhandled Rejection at: Promise', p, 'reason:', reason);
});

// TODO: Güvenlik önlemleri kapsamında burasının gözden geçirilmesi gerekli.
app.all('/*', function (req, res, next) { // Hangi verb ile gelirse gelsin tüm requestler için bu headerları ekle...
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With,Content-Type");
    next();
});

app.use(express.static(__dirname + '/public'));

doDontController(app);
booksController(app);

const serverInstance = app.listen(app.get('port'), function () {
    console.log("Mesaj : " + process.env.MESAJ);
    console.log("Express server is running on", app.get('port'));
});

serverInstance.timeout = 10 * 60 * 1000; // 10 dakika...
