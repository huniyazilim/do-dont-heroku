
document.addEventListener("click", () => {
    // alert("Bu programda robotu hareket ettirmek için yön tuşlarını kullanınız.");
});

const robot = document.querySelector(".robot"); // git bana sayfanın içinden robotu bul.
const kedi = document.querySelector(".kedi");
const maske = document.querySelector(".maske");
const gozler = document.querySelectorAll(".goz");

const sinirKontrol = () => {
    const {top, left} = robot.getBoundingClientRect(); // Robotun anlık yer bilgisini al

    if (left < 0) robot.style.marginLeft = "0px";
    if (top < 0) robot.style.marginTop = "0px";
}

// Basılan yön tuşlarına göre hareket ettiren kod.
document.addEventListener("keydown", (e) => {
    const basilanTus = e.key;
    console.log(basilanTus);
    const {top, left} = robot.getBoundingClientRect(); // Robotun anlık yer bilgisini al.

    if (basilanTus == " ") {
        robot.style.marginTop =(top - 200) + "px";
    }

    if (basilanTus == "Enter") {
        robot.style.marginTop = (top - 200) + "px";
        setTimeout( () => {
            robot.style.marginTop = top + "px";
        },1000);
    }

    if (basilanTus == "ArrowDown") robot.style.marginTop = (top + 50) + "px";
    if (basilanTus == "ArrowUp") robot.style.marginTop = (top - 50) + "px";
    if (basilanTus == "ArrowLeft") robot.style.marginLeft = (left - 50) + "px";
    if (basilanTus == "ArrowRight") robot.style.marginLeft = (left + 50) + "px";

    sinirKontrol();

});

const renkler = ["blue", "yellow", "black", "silver", "purple", "lime", "white", "pink", "lightgray", "red", "#f2c724"];

const sure = 200;

const renkDegistir = (renkNo) => {
    kedi.style.backgroundColor = renkler[renkNo];
    renkNo = renkNo + 1;
    if (renkNo < renkler.length) {
        setTimeout(() => {
            renkDegistir(renkNo);
        }, sure);
    }
}

kedi.addEventListener("click", (e) => {
    e.stopPropagation();
    setTimeout(() => {
        renkDegistir(0);
    }, sure);
});

const gozKirp = () => {
    setTimeout(() => {
        gozler.forEach((goz, i) => {
            goz.style.backgroundColor = "black";
        });
        setTimeout(() => {
            gozler.forEach((goz, i) => {
                goz.style.backgroundColor = "turquoise";
            });
        }, sure);
    }, sure);
}

maske.addEventListener("click", (e) => {
    e.stopPropagation();
    gozKirp();
});
