"use strict";

const {fbRef, getData} = require('../firebase/index.js');

const booksController = (app) => {

    app.post("/getBooks", function(req, res){

        const {userName} = req.body;
        const refStr = `users/${userName}/books/`;

        getData(refStr)
            .then((data) => {
                res.status(200).send(data);
            });

    });

    app.post("/deleteBook", function(req, res){

        const {userName, id} = req.body;
        const refStr = `users/${userName}/books/${id}`;

        fbRef.child(refStr)
            .set(null) 
            .then(() => res.status(200).send(id));

    });    

    app.post("/saveBook", function(req, res){

        const {userName, book} = req.body;
        const refStr = `users/${userName}/books`;

        const newBookRef = fbRef.child(refStr).push();
        const bookObj = JSON.parse(book);
        bookObj.id = newBookRef.getKey();

        newBookRef.set(bookObj)
            .then( () => res.status(200).send(bookObj) );

    });    

}

module.exports = booksController;
