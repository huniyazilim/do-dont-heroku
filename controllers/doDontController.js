"use strict";

const axios = require('axios');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const {fbRef, getData} = require('../firebase/index.js');
const {calculateTotalPercentage, calculateChain} = require("../firebase/calculations");

const doDontController = (app) => {

    app.get('/test', (req, res) => {
        const gizliAnahtar = process.env.GIZLI_ANAHTAR;
        console.log(`UserName : ${req.query.userName}`);
        const userData = {
            userName : req.query.userName,
            userType : "Basic",
            address : {
                il : "İstanbul",
                ilce : "Bahcelievler"
            }
        }

        jwt.sign(userData, gizliAnahtar, {expiresIn: "24h"}, 
            (err, token) => {
                if (err) {
                    res.status(500).send(err.toString());
                    return;
                }      
                console.log(token);
                userData.token = token;
                res.status(200).send(userData);
          });
        
    });
    
    app.post('/test', (req, res) => {
        var userName = req.body.userName;
        var password = req.body.password;

        var userToken = req.body.userToken;

        const gizliAnahtar = process.env.GIZLI_ANAHTAR;

        jwt.verify(userToken, gizliAnahtar, (err, userData) => {
            
            if (err) {
                res.status(500).send(err.toString());
                return;
            }

            const x_perm = req.headers['x-perm'];
            const x_nadir = req.headers['x-nadir'];
            const x_olcay = req.headers['x-olcay'];

            res.status(200).send({
                fromNadir : req.body.fromNadir,
                userName,
                password,
                x_perm,
                x_nadir,
                x_olcay,
                userData
            });

        });

    });

    app.post("/sendNotification", (req, res) => {
        // Eğer bahsi geçen ortam değişkeni mevcut değil ise sonuç undefined dönüyor.
        const key = process.env.BILDIRIM_ANAHTARI;

        if (!key){
            res.status(500).send("No notification key!");
            return;
        }

        const to = req.body.to || 'eeaTXCMqppc:APA91bEIGQadyesYGsKWktFUdl_fB2-X5Ib6cb-nU0aZwnkYu0buhKedzjVuT8o-7PoqQC6aaeVPTUw86EgqgserNQL1qq19jISXByHwdJRkSmwEfosKY1P5QL2VSPncHvXLZtla9eI1';

        const notification = {
          'title': req.body.title || 'Do-Dont App',
          'body': req.body.body || '7 to 2',
          'click_action': 'https://do-dont.herokuapp.com/'
        };

        const url = 'https://fcm.googleapis.com/fcm/send';

        axios.post(url,
          JSON.stringify({
            'notification': notification,
            'to': to
          }),
          {
            headers : {
              'Authorization': 'key=' + key,
              'Content-Type': 'application/json'
            }
          }
      );

      res.status(200).send("Notification sent successfuly!");
    });    

    app.post("/totalPercentage", async (req, res) => {
        const {userName, itemId, itemType, dateStr} = req.body;
        if (!userName || !itemId || !itemType || !dateStr) {
            res.send("Missing or empty arguments");
        }
        res.status(200).send({percent : await calculateTotalPercentage(userName, itemId, itemType, dateStr)});
    });    

    app.post("/chain", async (req, res) => {
        const {userName, itemId, itemType, dateStr} = req.body;
        if (!userName || !itemId || !itemType || !dateStr) {
            res.status(400).send("Missing or empty arguments");
        }
        res.status(200).send(await calculateChain(userName, itemId, itemType, dateStr));
    });

    app.post("/getEntries", (req, res) => {
        const {userName} = req.body;
        const refStr = `users/${userName}/list1`;
        getData(refStr)
            .then((data) => {
                res.status(200).send(data);
            });
    });    

    app.post("/saveNewEntry", (req, res) => {
  
        const {userName} = req.body;
        const doEntries = JSON.parse(req.body.doEntries);
        const dontEntries = JSON.parse(req.body.dontEntries);
        const dateObj = JSON.parse(req.body.dateObj);

        const refStr = `users/${userName}/list1/items/entries/` + dateObj.dateStr;
  
        const objToBeSaved = {
          does : doEntries,
          doesPercent : 0,
          donts : dontEntries,
          dontsPercent : 0,
          saveDate : dateObj.jsTime,
          saveDateStr : dateObj.dateStrP
        }

        fbRef.child(refStr)
            .set(objToBeSaved)
            .then(() => {
                res.status(200).send(objToBeSaved);
            });
    });      

    app.post("/userExists", (req, res) => {
        const {userName} = req.body;
        const refStr = `users/${userName}`;
        getData(refStr)
            .then((result) => {
                res.status(200).send(result!=null);
            });
    });    

    app.post("/createUser", (req, res) => {
        const {userName, userPass} = req.body;

        const saltRounds = 10;
        
        bcrypt.genSalt(saltRounds, function (err, salt) {
            if (err) {
              throw err;
            } else {
              bcrypt.hash(userPass, salt, function(err, hash) {
                if (err) {
                  throw err;
                } else {
                  console.log(hash)
                  const refStr = `users/${userName}`;

                  fbRef.child(refStr)
                      .set({password : hash})
                      .then(()=> {
                          res.status(200).send(userName);
                      });
                }

              });
            }
          });    

    });

    app.post("/isPasswordValid", (req, res) => {

        const {userName, userPass} = req.body;
        const refStr = `users/${userName}/password/`;

        getData(refStr)
            .then((result) => {
                const hash=result;
                if (hash) {
                    bcrypt.compare(userPass, hash, function(err, isMatch) {
                        if (err) {
                          throw err;
                        } 
                        // isMatch'e göre istenilen yapılabilir...
                        res.status(200).send({
                            userName,
                            isPasswordValid: isMatch
                        });
                      });

                } else {
                    res.status(200).send({
                        userName,
                        isPasswordValid: false
                    });
                }

        });
        
    });

    app.post("/getListItems", (req, res) => {
        const {userName, itemType} = req.body;
        const refStr = `users/${userName}/list1/items/${itemType}`;
        getData(refStr)
            .then((data) => {
                res.status(200).send(data);
            });
    });    

    app.post("/saveListItem", (req, res) => {
        const {userName, itemType, itemText} = req.body;
        const refStr = `users/${userName}/list1/items/${itemType}`;
        const newDoItemRef = fbRef.child(refStr).push();
        newDoItemRef.set(itemText)
            .then(() => {
                res.status(200).send({fbKey : newDoItemRef.getKey(), itemText});
            });
    });    

    app.post("/deleteListItem", function(req, res){

        const {userName, itemType, fbKey} = req.body;
        const refStr = `users/${userName}/list1/items/${itemType}/${fbKey}`;

        fbRef.child(refStr)
            .set(null) 
            .then(() => res.status(200).send(fbKey));

    });    

    app.post("/updateEntries", (req, res) => {

        const {userName, percentage, itemType} = req.body;
        const items = JSON.parse(req.body.items);
        const dateObj = JSON.parse(req.body.dateObj);

        const refStr = `users/${userName}/list1/items/entries/` + dateObj.dateStr;

        const updates = {};

        if (itemType == "does") {
            updates[refStr + "/does"] = items;
            updates[refStr + "/doesPercent"] = percentage;
        } else {
            updates[refStr + "/donts"] = items;
            updates[refStr + "/dontsPercent"] = percentage;
        }

        updates[refStr + "/saveDate"] = dateObj.jsTime;
        updates[refStr + "/saveDateStr"] = dateObj.dateStrP;

        fbRef.update(updates)
            .then(() => {
                res.status(200).send(updates); 
            });

    });   

    app.post("/getFCMToken", (req, res) => {
        const {userName} = req.body;
        const refStr = `users/${userName}/fcmToken`;

        getData(refStr)
            .then((data) => {
                res.status(200).send(data);
            });
            
    });    

    app.post("/saveFCMToken", (req, res) => {
        const {userName, fcmToken} = req.body;

        const refStr = `users/${userName}/fcmToken`;

        fbRef.child(refStr)
            .set(fcmToken)
            .then(()=> {
                res.status(200).send(fcmToken);
            });

    });
    
}

module.exports = doDontController;
