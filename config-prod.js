module.exports = {
    config :  {
        debugMode : false,
        userName : "Nadir",
        userId : 1,
        firebaseCongif : {
            apiKey: process.env.fb_apiKey,
            authDomain: process.env.fb_authDomain,
            databaseURL: process.env.fb_databaseURL,
            projectId: process.env.fb_projectId,
            storageBucket: process.env.fb_storageBucket,
            messagingSenderId: process.env.fb_messagingSenderId,
            appId: process.env.fb_appId
        },
        notificationConfig : {
            key : process.env.notify_key,
            url : process.env.notify_url,
            default_to : process.env.notify_default_to
        }
    }
}
