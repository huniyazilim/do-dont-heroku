"use strict";

const utils = require('../Utils/utils');
const {fbRef, getData} = require('./index.js');


const calculateTotalPercentage = (userName, itemId, itemType, dateStr) => {

    return new Promise(function(resolve, reject) {
        const entriesRef = `users/${userName}/list1/items/entries`;
        getData(entriesRef)
            .then((entries) => {

                const filterDate = utils.getJsTime(dateStr);
                let checkedCount = 0;
                let foundItemCount = 0;

                utils.objToArray(entries).forEach((entry, i) => {
                    if (entry.saveDate<=filterDate) {
                        // entry[itemType] o güne ait does ya da donts dizisini dönüyor...
                        const foundItem = entry[itemType].filter(e => e.fbKey==itemId); // o dizinin içinden ilgili öğeyi bul...
                        if (foundItem.length>0) {
                            // flattenedData.push(foundItem[0]);
                            foundItemCount = foundItemCount + 1;
                            checkedCount = foundItem[0].checked ? checkedCount + 1 : checkedCount;
                        }
                    }
                });

                const result = Math.round(checkedCount / foundItemCount * 100);

                if (!utils.isToday(dateStr)){
                    let saveRef = `users/${userName}/list1/items/entries/${dateStr.replace(/\./g,"_")}/${itemType}`;
                    getData(saveRef)
                        .then((data) => {
                            const idx = data.findIndex(entry => entry.fbKey == itemId);
                            if (idx!=-1) {
                                const updateRef = saveRef + "/" + idx + "/totalPercentage";
                                const updateObj = {};
                                updateObj[updateRef] = result;
                                // Burada hesaplanan totalPercentage bilgisini ilgili yere set et!
                                fbRef.update(updateObj);
                            }
                        });
                }

                resolve(result);

            });
    });

}

const calculateChain = (userName, itemId, itemType, dateStr) => {
    return new Promise(function(resolve, reject) {
        const entriesRef = `users/${userName}/list1/items/entries`;
        getData(entriesRef)
            .then((entries) => {

                const filterDate = utils.getJsTime(dateStr);
                let chainCount = 0;
                let chainBroken = false;

                const sortedArr = utils.objToArray(entries).sort((a, b) => b.saveDate - a.saveDate);

                sortedArr.forEach((entry) => {
                    if (entry.saveDate<=filterDate) {
                        const foundItem = entry[itemType].filter(e => e.fbKey==itemId); // o dizinin içinden ilgili öğeyi bul...
                        if (foundItem.length>0) {
                            foundItem[0].dateStr = entry.saveDateStr;
                            if (!chainBroken && foundItem[0].checked) {
                                chainCount = chainCount + 1;
                            } else {
                                chainBroken = true;
                            }
                        }
                    }
                })

                if (!utils.isToday(dateStr)){
                    let saveRef = `users/${userName}/list1/items/entries/${dateStr.replace(/\./g,"_")}/${itemType}`;
                    getData(saveRef)
                        .then((data) => {
                            const idx = data.findIndex(entry => entry.fbKey == itemId);
                            if (idx!=-1) {
                                const updateRef = saveRef + "/" + idx + "/chainCount";
                                const updateObj = {};
                                updateObj[updateRef] = chainCount;
                                // Burada hesaplanan chainCount bilgisini ilgili yere set et!
                                fbRef.update(updateObj);
                            }
                        });
                }

                resolve({chainCount});
            });
    });

}

module.exports = {
    calculateTotalPercentage,
    calculateChain
}
